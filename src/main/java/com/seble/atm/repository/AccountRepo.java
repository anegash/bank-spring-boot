package com.seble.atm.repository;

import org.springframework.data.repository.CrudRepository;

import com.seble.atm.model.Account;

public interface AccountRepo extends CrudRepository<Account, Long> {

}

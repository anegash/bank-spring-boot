package com.seble.atm.repository;

import org.springframework.data.repository.CrudRepository;

import com.seble.atm.model.Customer;

public interface CustomerRepo extends CrudRepository<Customer, Long> {

}

package com.seble.atm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seble.atm.model.Customer;
import com.seble.atm.repository.CustomerRepo;

@Service
public class CustomerService {

	@Autowired
	private CustomerRepo customerRepo;

	public Customer createCustomer(Customer customer) {
		return customerRepo.save(customer);

	}

	public Customer getCustomer(Long id) {
		return customerRepo.findById(id).get();
	}
}

package com.seble.atm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seble.atm.model.Account;
import com.seble.atm.model.Customer;
import com.seble.atm.repository.AccountRepo;
import com.seble.atm.repository.CustomerRepo;

@Service
public class AccountService {

	@Autowired
	private AccountRepo accountRepo;

	@Autowired
	private CustomerRepo customerRepo;

	public Account createAccount(Long customerId) {
		Customer customer = customerRepo.findById(customerId).get();
		Account account = new Account();
		account.setCustomer(customer);
		return accountRepo.save(account);
	}

}

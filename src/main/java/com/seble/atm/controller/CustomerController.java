package com.seble.atm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.seble.atm.model.Customer;
import com.seble.atm.service.CustomerService;

@Controller
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@GetMapping("/")
	private String home() {
		return "home";
	}

	@GetMapping("/create")
	private String create() {
		return "customer/create";
	}

	@GetMapping("/login")
	private String login() {
		return "customer/create";
	}

	@GetMapping("/customer/{id}/home")
	private String create(@PathVariable Long id, Model model) {
		Customer customer = customerService.getCustomer(id);
		model.addAttribute("customer", customer);
		return "customer/home";
	}

	@PostMapping("/create")
	private String create(@ModelAttribute Customer customer, Model model) {
		customer = customerService.createCustomer(customer);
		model.addAttribute("customer", customer);
		return "customer/home";
	}

}

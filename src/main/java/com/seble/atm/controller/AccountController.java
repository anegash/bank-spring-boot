package com.seble.atm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.seble.atm.model.Account;
import com.seble.atm.service.AccountService;

@Controller
@RequestMapping("/account")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@RequestMapping("/create")
	private String createAccount(@RequestParam Long customerId, Model model) {
		Account account = accountService.createAccount(customerId);
		model.addAttribute("account", account);
		return "account/accountHome";

	}

}
